# AryaLinux Build Scripts

This repository is contains the build scripts for building AryaLinux from scratch or to build your own Linux distribution(on the same lines as AryaLinux) if you wish so.

The project contains the following directories:

1) base-system - Which builds the base system(LFS loosely speaking). The base system comprises of gcc, glibc, kernel and the toolchain necessary to build the rest of the system.
2) applications - Applications which can be built on top of the base system or packages if you wish to call them that. Each script in applications corresponds to a single package and is usually downloaded and built from source(except a few ones whose binaries are directly downloaded and extracted to the filesystem)

In order to start the build process first switch to the branch which you want to build. Each branch corresponds to a version of AryaLinux. The latest release branch of AryaLinux is 2.4 for instance. After switching branch you may run the ./build-arya script and it would take you through the rest of the build process.

To learn more about the build process visit: http://aryalinux.info/help.php?article=building
